
<br />
<p align="center">
  <a href="https://gitlab.com/peuybul11/app-tools/python/youtube-downloader">
    <img src="https://i.ibb.co/HNdyZCS/youtube.png" >
  </a>

  <h3 align="center">Youtube Downloader</h3>

  <p align="center">
    Pengunduh Video Youtube 🎥 Sederhana yang dikembangkan menggunakan Pytube (Perpustakaan Python) 
    <br />
    
  </p>
</p>

## Tentang Project

Youtube Downloader adalah Aplikasi Python yang dapat digunakan untuk mendownload Video Youtube. Pengunduh Youtube dikembangkan menggunakan Pytube dan Tkinter.

## Technology Stack

* [Python](https://www.python.org/)
* [Pytube](https://python-pytube.readthedocs.io/en/latest/)
* [Tkinter](https://www.tutorialspoint.com/python/python_gui_programming.htm)
## Instalasi

1. Berikan ⭐ di Repositori Gitlab. 
2. Kloning Repositori dengan masuk ke Git Client lokal Anda dan masukan perintah: 

```sh
git clone https://gitlab.com/peuybul11/app-tools/python/youtube-downloader.git
```

3. Install the Packages: 
```sh
pip install --upgrade pytube
pip install requests
```
Jika Anda menghadapi masalah dalam mengunduh file Video/Audio Musik, maka:
```sh
Buka folder Pytube Anda dan buka file ekstrak.py lalu pada baris ke 301 (atau sesuatu di dekat baris ini)
ubah  "cipher" -> "signatureCipher"
```
4. Terakhir, lakukan perintah berikut:
```sh
python main.py
```

<!-- ## Connect with me
<a href="https://github.com/swapnilsparsh" target="_blank">
<img src=https://img.shields.io/badge/github-%2324292e.svg?&style=for-the-badge&logo=github&logoColor=white alt=github style="margin-bottom: 5px;" />
</a>
<a href="https://www.linkedin.com/in/swapnil-srivastava-sparsh/" target="_blank">
<img src=https://img.shields.io/badge/linkedin-%231E77B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white alt=linkedin style="margin-bottom: 5px;" />
</a> -->
